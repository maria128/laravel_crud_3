<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{

    protected $table = 'order';

    protected $fillable = [
        'date',
        'buyer'
    ];

    public function orderItem()
    {
        return $this->hasMany(OrderItem::class, 'order', 'id');
    }

    public function buyers(): BelongsTo
    {
        return $this->belongsTo(Buyer::class, 'buyer', 'id');
    }


    public function getDate()
    {
        return $this->date;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }

    public function getSum()
    {
        return collect($this->orderItem)->sum('price') / 100;
    }
}