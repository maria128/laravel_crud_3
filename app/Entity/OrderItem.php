<?php

declare(strict_types=1);

namespace App\Entity;

use App\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderItem extends Model
{

    private $sum;


    protected $table = 'order_item';

    protected $fillable = [
        'product',
        'quantity',
        'price',
        'discount',
        'order',
    ];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'order', 'id');
    }

    public function products(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product', 'id');
    }


    public function getId()
    {
        return $this->id;
    }


    public function getProduct()
    {
        return $this->product;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDiscount()
    {
        return $this->discount;
    }


    public function getSum()
    {
        return ($this->price * $this->discount/100 * $this->quantity) / 100;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }
}