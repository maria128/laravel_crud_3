<?php

use App\Entity\Order;
use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $orders = Order::all();

        $products = Product::all();


        $orderItems = $orders->map(
            function (Order $order) use ($products) {
                $productsIds = $products->shuffle()->shuffle()->take(10)->pluck('id');

                return $productsIds->map(
                    function(int $productId) use ($order) {
                        return factory(\App\Entity\OrderItem::class, 1)->make([
                            'product' => $productId,
                            'order' => $order->id,
                        ]);
                    }
                );

            }
        );
        DB::table('order_item')->insert($orderItems->flatten()->toArray());

    }
}